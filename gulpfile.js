var _if          = require('gulp-if');
var argv         = require('yargs').argv;
var autoprefixer = require('gulp-autoprefixer');
var babel        = require('gulp-babel');
var browserSync  = require('browser-sync');
var concat       = require('gulp-concat');
var cssmin       = require('gulp-cssmin');
var fs           = require('fs');
var gulp         = require('gulp');
var imagemin     = require('gulp-imagemin');
var less         = require('gulp-less');
var panini       = require('panini');
var rename       = require('gulp-rename');
var rimraf       = require('rimraf');
var sass         = require('gulp-sass');
    sass.compiler = require('node-sass');
var streamqueue  = require('streamqueue');
var uglify       = require('gulp-uglify');


var config = {
    assets:     './assets/',
    dist:       './dist/',
    pages:      './assets/pages/',
    port:       8000,
    production: (argv.production === undefined) ? false : true,
    root:       './'
}

// Remove a folder
var clean = function(folder, done) {
    rimraf(folder, done);
};

// Run a server to see changes
gulp.task('server', function() {
	browserSync.init({
		server: config.dist,
        port:   config.port
	});
});

// Watch folders for updates
gulp.task('watch', function() {
    gulp.watch(config.assets+'{pages,layouts,partials}/**/*.{html,hbs,handlebars}', ['pages']);
    gulp.watch(config.assets+'{scss,less,css}/*.{scss,less,css}', ['css']);
    gulp.watch(config.assets+'js/*.js', ['javascript']);
    gulp.watch(config.assets+'img/*', ['images']);
});

// Task to build pages
// Uses panini
gulp.task('pages', function() {
    panini.refresh();

	gulp
    .src(config.assets + 'pages/**/*.{html,hbs,handlebars}')
	.pipe(panini({
		root:     config.assets + 'pages/',
		layouts:  config.assets + 'layouts/',
		partials: config.assets + 'partials/'
	}))
	.pipe(gulp.dest(config.dist))
    .pipe(browserSync.reload({stream: true}));
});

// Task for css files
// Automatically detects if CSS, SASS or LESS
// Runs some actions only for production
gulp.task('css', function() {
    var type = 'css';

    if(fs.existsSync(config.assets+'scss')) {
        type = 'scss';
    } else if(fs.existsSync(config.assets+'less')) {
        type = 'less';
    }

    gulp
    .src(config.assets+type+'/styles.'+type)
    .pipe(_if(type === 'scss', sass().on('error', sass.logError)))
    .pipe(_if(type === 'less', less()))
    .pipe(_if(config.production, cssmin()))
    .pipe(_if(config.production, autoprefixer()))
    .pipe(rename('styles.css'))
    .pipe(gulp.dest(config.dist+'css/'))
    .pipe(browserSync.reload({stream: true}));
});

// Task for javascript
// Runs some actions only for production
gulp.task('javascript', function() {
    return streamqueue({objectMode: true},
        gulp.src(config.assets+'js/!(main)*.js'), 
        gulp.src(config.assets+'js/main.js')
    )
    .pipe(babel())
    .pipe(concat('main.js'))
    .pipe(_if(config.production, uglify()))
    .pipe(gulp.dest(config.dist+'js/'))
    .pipe(browserSync.reload({stream: true}));
});

// Task to copy images
gulp.task('images', function() {
    clean(config.dist+'img', function() {
        gulp
        .src(config.assets+'img/*.{gif,jpg,png,svg}')
        .pipe(_if(config.production, imagemin()))
        .pipe(gulp.dest(config.dist+'img/'))
        .pipe(browserSync.reload({stream: true}));
    });
});

gulp.task('default', ['pages', 'css', 'javascript', 'images', 'server', 'watch']);
gulp.task('build',   ['pages', 'css', 'javascript', 'images']);
