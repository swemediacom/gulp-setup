# Boilerplate Gulp Setup

This is a boilerplate gulp setup for frontend projects. It comes with some common used ressources, installs all neccessary packages and provides useful gulp tasks.

## Requirements

Check that you have [Node.js](https://nodejs.org/en/download/) and [npm](https://www.npmjs.com/get-npm) installed:

```
node -v
```

```
npm -v
```

## Installing

In your terminal, navigate to the root folder of this project. Install all dependencies with the following command:

```
npm install
```

## Gulp Watch

You can use gulp watch to see all file changes inside the folder "assets" in real-time in your browser. It will start a server with the command:

```
npm start
```

## Build

You can build production files into the folder "dist" with the command:

```
npm run build
```

## CSS

The gulp task for CSS will automatically detect a folder "sass", "less" or "css" and will handle the CSS accordingly.

In each case there should be a file called "styles.<extension>" that combines all other CSS resources.

## Pages with Panini

You can build pages with the help of the package [Panini](https://www.npmjs.com/package/panini). It is useful if you have repeating partials or elements that you want to include in several pages.

For example you could create two files like this:

```
/pages/index.html

{{> header}}
```

```
/partials/header.html

<div>This is the header!</div>
```

This would create a page called index.html that included the partial header.html.

It is important that the name of the partial matches the filename.
